class UI {
  constructor() {
    this.mobileMenu = document.querySelector(".burger-menu");
    this.burgerBtnOpen = document.querySelector(".burger-btn-open");
    this.burgerBtnClose = document.querySelector(".burger-btn-close");
  }
}
export default UI;
