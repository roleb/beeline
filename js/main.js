import Swiper from "https://unpkg.com/swiper/swiper-bundle.esm.browser.min.js";
import UI from "./app.js";

const ui = new UI();
ui.burgerBtnOpen.addEventListener("click", function () {
  ui.mobileMenu.classList.remove("d-none");
  ui.mobileMenu.classList.add("d-block");
});

ui.burgerBtnClose.addEventListener("click", function () {
  ui.mobileMenu.classList.remove("d-block");
  ui.mobileMenu.classList.add("d-none");
});

const swiper = new Swiper(".swiper-container", {
  // Optional parameters
  direction: "horizontal",
  loop: true,
  spaceBetween: 16,
  // If we need pagination
  pagination: {
    el: ".swiper-pagination",
  },
  effect: "coverflow",
  coverflowEffect: {
    rotate: 30,
    slideShadows: false,
    stretch: 0,
    modifier: 6,
    depth: 0,
  },
});
